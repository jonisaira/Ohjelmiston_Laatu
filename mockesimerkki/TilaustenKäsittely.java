
package mockesimerkki;

public class TilaustenKäsittely {

    private Hinnoittelija_IF hinnoittelija;

    public void setHinnoittelija(Hinnoittelija_IF hinnoittelija) {
        this.hinnoittelija = hinnoittelija;
    }

    public void käsittele(Tilaus tilaus) {

        float alennusProsentti = hinnoittelija.getAlennusProsentti(
                tilaus.getAsiakas(), tilaus.getTuote());
        float alennusHinta = tilaus.getTuote().getHinta()
                * (1 - (alennusProsentti / 100));
        tilaus.getAsiakas().setSaldo(
                tilaus.getAsiakas().getSaldo() - alennusHinta);
    }

}
